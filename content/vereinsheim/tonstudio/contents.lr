_model: page
---
title: Tonstudio
---
content:

Wer unser Studio betritt, wird von vielen bunten Lichtern begrüßt. Sowohl von den Geräten selbst, als auch von Leuchtröhren, LED-Streifen, Lava-Lampe, oder durch atmosphärisch-psychedelische Videos vom Beamer. Sofern gerade an Klängen geschraubt wird, bekommt man obendrein auch akustisch etwas geboten - der mit einem schwarzen Vorhang versehene, höhlenartige Eingang um die Ecke sowie das Sofa laden jederzeit zum spontanen Zuhören ein. Wer mit synthetischen Klängen nicht so viel anzufangen weiß, der hat die Möglichkeit, unserer E-Gitarre ein paar schrille Akkorde zu entlocken.

Wer einen bleibenden Eindruck hinterlassen möchte, kann die klanglichen Experimente selbstverständlich aufzeichnen. Dies geschieht mithilfe unseres Studio-PCs, welcher vollgepackt mit OpenSource-Kreativsoftware und einem MIDI-Keyboard ausgestattet bereitsteht und per Audio-Interface mit unserem Mischpult verbunden ist. In letzterem ist übrigens auch immer genügend Platz für eigene, mitgebrachte Geräte. Ein Tapedeck und ein Plattenspieler bieten zusätzliche Möglichkeiten, sich kreativ auszuleben.

Ein paar Eindrücke aus unserem Tonstudio:
---------------------------------------
<span class="image fit"><img src="./tonstudio-von-links.jpg"></span><br/>
<span class="image fit"><img src="./tonstudio-mit-beamer-rechts.jpg"></span><br/>
<span class="image fit"><img src="./tonstudio-bergkette.jpg"></span><br/>
<span class="image fit"><img src="./tonstudio-frontal-zoomedout.jpg"></span><br/>
---
slogan: "Boing Boom Tschak"
---
teaser: Das Toolbox-Tonstudio: Vom Synthesizer über Sampler und Drum Machines bis hin zur E-Gitarre ist hier alles zu finden, was das Producer-Herz begehrt.
